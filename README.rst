.. You should enable this project on travis-ci.org and coveralls.io to make
   these badges work. The necessary Travis and Coverage config files have been
   generated for you.

.. image:: https://travis-ci.org/memaldi/ckanext-rating.svg?branch=master
    :target: https://travis-ci.org/memaldi/ckanext-rating

.. image:: https://coveralls.io/repos/memaldi/ckanext-rating/badge.png?branch=master
  :target: https://coveralls.io/r/memaldi/ckanext-rating?branch=master

=============
ckanext-rating
=============

A frontend for the CKAN rating system.

------------
Requirements
------------

* CKAN 2.4.1

------------------------
Development Installation
------------------------

To install ckanext-rating for development, activate your CKAN virtualenv and
do::

    git clone https://github.com/memaldi/ckanext-rating.git
    cd ckanext-rating
    python setup.py develop
    pip install -r dev-requirements.txt
